echo "Saving local checkpoint..."
cd ./storage/
git -a -m "CREATE LAUNCH CHECKPOINT"
git
echo "Saving local checkpoint...DONE"

echo "Launching GnuCash..."
docker run -it dolphin/gnucash:latest
CONTAINER_ID=`docker ps -lq
echo "Exiting GnuCash..."
echo "Exiting GnuCash...DONE"

echo "Synchronizing storage with $CONTAINER_ID..."
docker cp $CONTAINER_ID:/app/output/ ./storage/
echo "Removing files from $CONTAINER_ID...DONE"









rm -rf ./staging/$STAGING_DIR
echo "Removing files from $CONTAINER_ID..."
echo "The next step in this script will destroy the container.  If you do not want this to occur, press 
CTL+C.  Otherwise press Enter."
echo "You can remove the container later by running >docker rm $CONTAINER_ID"
read PAUSEGET
echo "DESTROYING CONTAINER $CONTAINER_ID..."
docker rm $CONTAINER_ID
echo "DESTROYING CONTAINER $CONTAINER_ID...DONE"
echo "Your mbox file should now be in this directory."
